from miniops.agent import MinioAgent
import pandas as pd

class Watcher(MinioAgent):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process(self, dataframe: pd.DataFrame):
        print(dataframe.head)
