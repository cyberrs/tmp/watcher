import os
from watcher.watcher import Watcher

agent = Watcher(
    endpoint=os.environ["MINIO_ENDPOINT"],
    access_key=os.environ["MINIO_ACCESS_KEY"],
    secret_key=os.environ["MINIO_SECRET_KEY"],
    secure=False,
    name="minio-watcher"
)

while True:
    agent.run(bucket_name=os.environ["MINIO_BUCKET_NAME"])
