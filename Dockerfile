FROM python:3-slim

WORKDIR /app
COPY watcher watcher
COPY pyproject.toml .
RUN pip install .

ENTRYPOINT [ "python" ]
CMD [ "/app/run.py" ]
